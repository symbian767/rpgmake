#pragma once

struct ARMORPAR
{
	char Name[50];
	int PlusHP;

	int Nalichie;
};

struct ARMOR
{
	ARMORPAR Leather = { "������� �����", 500, 0 };
	ARMORPAR Rudara = { "����� ������" , 650, 1 };
	ARMORPAR Djeya = { "����� ������������� �����", 600, 1 };
};

ARMOR Armor;

struct WeaponParametr
{
	char Name[50];
	int Damage;
	int Nalichie;
};

struct WEAPON // + � ����� � �������� �����
{
	WeaponParametr W001 = { "�������� ���", 50, 0 };
	WeaponParametr RudaraWeapon = { "��� ������", 65, 1 };
	WeaponParametr VoinWeapon = { "��� ������������� �����", 60, 1 };

};

WEAPON Weapon;

struct QuestLootChar
{
	char Name[50];
	int Nalichie;
};

struct QuestLoot
{
	QuestLootChar KulonSPhoto = { "����� � �����������", 0 };
	QuestLootChar Verevka = { "�������", 0 };
	QuestLootChar Tesma = { "������", 0 };
	QuestLootChar LetterDjeyadaev = { "������� � ������� ���������", 0 };
	QuestLootChar Kluchnisa = { "��������", 0 };
	QuestLootChar PismoSPechatiyuFiakranii = { "������ � ������� ���������", 0 };
	QuestLootChar KeyForPodvar = { "���� �� �������", 0 };
};

QuestLoot Loot;

struct Player
{
	char username[20];
	int healthPoint;
	int Strength;
	int Perception;
	int Endurance;
	int Charisma;
	int Intelligence;
	int Agility;
	int Lucky;
	int WeaponDamage;
	int ArmorHP;

	int Money;
};

Player Hero = { "������" };

struct MOB
{
	Player Rat = { "�����", 500, 3, 6, 4, 1, 5, 8, 4 };
	Player GiantSpider = { "���������� ����", 450, 5, 7, 1, 1, 4, 6, 3 };
	Player Goblin = { "������", 780, 4, 4, 5, 2, 5, 8, 3 };
	Player Bandit = { "������", 1000 , (rand() % 3 + 3), (rand() % 3 + 2),(rand() % 3 + 4), (rand() % 3 + 1), (rand() % 3 + 3), (rand() % 3 + 5), (rand() % 3 + 3) };
	Player BanditVor = { "������-�������", 1000 , (rand() % 3 + 1), (rand() % 3 + 3),(rand() % 3 + 1), (rand() % 3 + 2), (rand() % 3 + 3), (rand() % 3 + 7), (rand() % 3 + 3) };
	Player Rudara = { "������", 800, 6, 5, 4, 3, 5, 7, 4, Weapon.RudaraWeapon.Damage, Armor.Rudara.PlusHP };
	Player Strazhnik = { "����������� ����", 800, 7,2,5,3,1,3,2,Weapon.VoinWeapon.Damage, Armor.Djeya.PlusHP };
};
MOB Mob;