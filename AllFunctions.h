#pragma once

#include <iostream>
#include <ctime>
#include "Content.h"
#include <windows.h>
#include <cstdlib>
#include <conio.h>
#include <mmsystem.h>
#include "resource.h"
#include "Act_I_Scenarios.h"

using namespace std; 

void QuickAttack(Player& _Attack, Player& _Defeat);
void HardAttack(Player& _Attack, Player& _Defeat);
void FastAttack(Player& _Attack, Player& _Defeat);
//void Defense(Player& _Attack, Player& _Defeat);
int Battle(Player& _noob, Player _mob);
void DistributeAttributes(Player& _noob);
void Message(const char _str[]);
void Intro();
void Gameover(char A[]);


void Message(const char _str[])
{
	for (int i = 0; i < strlen(_str); i++, Sleep(50))
	{
		cout << _str[i];
	}
	Sleep(700);
	cout << endl << endl;
}
void DistributeAttributes(Player& _noob)
{

	Message(A0000);
	Message(A0001);
	Message(A0002);
	Message(A0003);
	Message(A0004);
	int NumOchk = 40, PotrOchk = 0, Superfluous = 0;
	cout << "��������������! ������� �� ���� ��������� ������ �� ��������� ������� ��������. ������� ��� �� ����������." << endl;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [����] :";
	cin >> _noob.Strength;
	while (_noob.Strength == 0 || _noob.Strength > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [����] :";
		cin >> _noob.Strength;
	}
	PotrOchk = PotrOchk + _noob.Strength;
	NumOchk = NumOchk - _noob.Strength;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [����������] :";
	cin >> _noob.Perception;
	while (_noob.Perception == 0 || _noob.Perception > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [����������] :";
		cin >> _noob.Perception;
	}
	PotrOchk = PotrOchk + _noob.Perception;
	NumOchk = NumOchk - _noob.Perception;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [������������] :";
	cin >> _noob.Endurance;
	while (_noob.Endurance == 0 || _noob.Endurance > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [������������] :";
		cin >> _noob.Endurance;
	}
	PotrOchk = PotrOchk + _noob.Endurance;
	NumOchk = NumOchk - _noob.Endurance;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [�������] :";
	cin >> _noob.Charisma;
	while (_noob.Charisma == 0 || _noob.Charisma > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [�������] :";
		cin >> _noob.Charisma;
	}
	PotrOchk = PotrOchk + _noob.Charisma;
	NumOchk = NumOchk - _noob.Charisma;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [���������] :";
	cin >> _noob.Intelligence;
	while (_noob.Intelligence == 0 || _noob.Intelligence > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [���������] :";
		cin >> _noob.Intelligence;
	}
	PotrOchk = PotrOchk + _noob.Intelligence;
	NumOchk = NumOchk - _noob.Intelligence;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [��������] :";
	cin >> _noob.Agility;
	while (_noob.Agility == 0 || _noob.Agility > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [��������] :";
		cin >> _noob.Agility;
	}
	PotrOchk = PotrOchk + _noob.Agility;
	NumOchk = NumOchk - _noob.Agility;
	cout << "���������� �����: [" << NumOchk << "]" << endl;
	cout << "������� ���������� ����� ��� �������� [�����] :";
	cin >> _noob.Lucky;
	while (_noob.Lucky == 0 || _noob.Lucky > 10)
	{
		cout << "�� ������� �������� �������," << _noob.username << ". ������ �������� �� ����� �� ������!" << endl;
		cout << "������� ���������� ����� ��� �������� [�����] :";
		cin >> _noob.Lucky;
	}
	PotrOchk = PotrOchk + _noob.Lucky;
	NumOchk = NumOchk - _noob.Lucky;
	//--------------------------------------------------------- ����� ���� ���� � ��������
	cout << "������ ����� ���������:" << endl;
	srand(time(0));
	_noob.healthPoint = 800 + (rand() % 23 + 3 * _noob.Endurance);
	cout << "[���� �����] = [" << _noob.healthPoint << "]" << endl;
	cout << "[����] = [" << _noob.Strength << "]" << endl;
	cout << "[����������] = [" << _noob.Perception << "]" << endl;
	cout << "[������������] = [" << _noob.Endurance << "]" << endl;
	cout << "[�������] = [" << _noob.Charisma << "]" << endl;
	cout << "[���������] = [" << _noob.Intelligence << "]" << endl;
	cout << "[��������] = [" << _noob.Agility << "]" << endl;
	cout << "[�����] = [" << _noob.Lucky << "]" << endl << endl;
	cout << "��������� ������ : |" << PotrOchk << "|" << endl;
	system("pause");
	int NumberOfPointsDeducted = 0;
	if (PotrOchk > 40)
	{
		Superfluous = PotrOchk - 40;
		do
		{
			cout << "�� ��������� ����� ��������� �����!/n��� ����� ������ " << Superfluous << " �����!" << endl;
			cout << "���������, ������ ����� ��������� ����� :" << endl;
			cout << "1. ����" << endl;
			cout << "2. ����������" << endl;
			cout << "3. ������������" << endl;
			cout << "4. �������" << endl;
			cout << "5. ���������" << endl;
			cout << "6. ��������" << endl;
			cout << "7. �����" << endl;
			char Choise;
			Choise = _getch();
			switch (Choise)
			{
			case 49:
			{
				cout << "������� ����� ���� ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Strength -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[����] = [" << _noob.Strength << "]" << endl << endl;
				break;

			}
			case 50:
			{
				cout << "������� ����� ���������� ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Perception -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[����������] = [" << _noob.Perception << "]" << endl << endl;
				break;

			}
			case 51:
			{
				cout << "������� ����� ������������ ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Endurance -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[������������] = [" << _noob.Endurance << "]" << endl << endl;
				break;
			}
			case 52:
			{
				cout << "������� ����� ������� ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Charisma -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[�������] = [" << _noob.Charisma << "]" << endl << endl;
				break;
			}
			case 53:
			{
				cout << "������� ����� ���������� ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Intelligence -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[���������] = [" << _noob.Intelligence << "]" << endl << endl;
				break;
			}
			case 54:
			{
				cout << "������� ����� �������� ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Agility -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[��������] = [" << _noob.Agility << "]" << endl << endl;
				break;
			}
			case 55:
			{
				cout << "������� ����� ����� ������ �������?" << endl;
				cin >> NumberOfPointsDeducted;
				_noob.Lucky -= NumberOfPointsDeducted;
				Superfluous -= NumberOfPointsDeducted;
				cout << "[�����] = [" << _noob.Lucky << "]" << endl << endl;
				break;
			}
			}
			cout << "[����] = [" << _noob.Strength << "]" << endl;
			cout << "[����������] = [" << _noob.Perception << "]" << endl;
			cout << "[������������] = [" << _noob.Endurance << "]" << endl;
			cout << "[�������] = [" << _noob.Charisma << "]" << endl;
			cout << "[���������] = [" << _noob.Intelligence << "]" << endl;
			cout << "[��������] = [" << _noob.Agility << "]" << endl;
			cout << "[�����] = [" << _noob.Lucky << "]" << endl << endl;
			cout << "Number" << NumberOfPointsDeducted << endl;
			system("pause");
		} while (Superfluous != 0);
	}
}
int Battle(Player& _noob, Player _mob)
{
	PlaySound(MAKEINTRESOURCE(IDR_WAVE2), NULL, SND_RESOURCE | SND_ASYNC | SND_LOOP);
	char AttackPlayer;
	int AttackMob=0;
	int DefenseChance = 0;
	int DefenseChanceEnemy = 0;
	int DefenseResultPlayer = 0;
	int DefenseResultMob = 0;
	_noob.healthPoint+= _noob.ArmorHP;
	_mob.healthPoint += _mob.ArmorHP;
	do
	{
		AttackMob = 0;
		DefenseChance = 0;
		DefenseChanceEnemy = 0;
		DefenseResultPlayer = 0;
		DefenseResultMob = 0;
		system("cls");
		srand(time(0));
		cout << "|" << _noob.username << ":" << _noob.healthPoint << "|                        " << "|" << _mob.username << ":" << _mob.healthPoint << "|" << endl;
		Sleep(2000);

		if (DefenseResultMob == 0)
		{
			cout << "�������� ��� �����:" << endl << "[ 1) ������� ����]" << "         " << "[ 2) ������� ����]" << endl << "[ 3)������� ����]" << "         " << "[ 4) ������(����)]" << endl;
			AttackPlayer = _getch();
			switch (AttackPlayer)
			{
			case 49:
			{
				cout << _noob.username << " ���������� ������� ����" << endl;
				Sleep(400);
				QuickAttack(_noob, _mob);
				break;
			}
			case 50:
			{
				cout << _noob.username << " ���������� ������� ����" << endl;
				Sleep(400);
				HardAttack(_noob, _mob);
				break;
			}
			case 51:
			{
				cout << _noob.username << " ���������� ������� ����" << endl;
				Sleep(400);
				FastAttack(_noob, _mob);
				break;
			}
			case 52:
			{
				cout << _noob.username << " ���������� ������(����)" << endl;
				Sleep(400);
				int DefenseChance = 0;
				int DefenseChanceEnemy = 0;
				DefenseChance = rand() % 15 + 1 * rand() % _noob.Agility + 1 * rand() % _noob.Endurance + 1;
				DefenseChanceEnemy = rand() % 10 + 1 * rand() % _mob.Agility + 1 * rand() % _mob.Endurance + 1;
				if (DefenseChance > DefenseChanceEnemy)
				{
					cout << "������ ������ �������!" << endl;
					Sleep(400);
					DefenseResultPlayer = 1;
				}
				else
				{
					cout << "������ �� �������" << endl;
					Sleep(400);
				}
				break;
			}
			}
		}

		else
		{
			cout << _mob.username << " ��������� �� �����" << endl; Sleep(400);
		}

		if (_mob.healthPoint <= 0)
		{
			break;
		}

		Sleep(2000);
		
		if (DefenseResultMob == 0)
		{
			AttackMob = rand() % 4 + 1;
			switch (AttackMob)
			{
			case 1:
			{
				cout << _mob.username << " ���������� ������� ����" << endl; Sleep(400);
				QuickAttack(_mob, _noob);
				break;
			}
			case 2:
			{
				cout << _mob.username << " ���������� ������� ����" << endl; Sleep(400);
				HardAttack(_mob, _noob);
				break;
			}
			case 3:
			{
				cout << _mob.username << " ���������� ������� ����" << endl; Sleep(400);
				FastAttack(_mob, _noob);
				break;
			}
			case 4:
			{
				cout << _mob.username << " ���������� ������(����)" << endl; Sleep(400);
				int DefenseChance = 0;
				int DefenseChanceEnemy = 0;
				DefenseChance = rand() % 10 + 1 * rand() % _mob.Agility + 1 * rand() % _noob.Endurance + 1;
				DefenseChanceEnemy = rand() % 10 + 1 * rand() % _noob.Agility + 1 * rand() % _mob.Endurance + 1;
				if (DefenseChance > DefenseChanceEnemy)
				{
					cout << "������ ������ �������!" << endl; Sleep(400);
					DefenseResultMob = 1;
				}
				else
				{
					cout << "������ �� �������" << endl; Sleep(400);
				}
				break;
			}
			}
		}

		else
		{
			cout << _noob.username << " ��������� �� �����" << endl;
			Sleep(2000);
		}

		if (_noob.healthPoint <= 0)
		{
			break;
		}
		cout << endl << endl;
		Sleep(700);
	} while (_noob.healthPoint > 0 || _mob.healthPoint > 0);
	if (_noob.healthPoint <= 0)
	{
		cout << endl << "��� ���� " << _mob.username;
		return(5);
	}
	else
	{
		cout << endl << "�� ������� ���������� " << _mob.username << endl;
		cout << "� ��� ��������:|" << _noob.healthPoint << " HP|" << endl;
		Sleep(2000);
		system("cls");
		return(0);
	}
	system("pause");
}
void QuickAttack(Player& _Attack, Player& _Defeat)
{
	int AttackDamage = 0, MissOrNotMissAttack = 0, MissOrNotMissDefeat = 0, CriticalDamage = 0, CriticalOn = 0, AttackPhysicalDamage = 0;
	srand(time(0));
	CriticalOn = rand() % 16;
	if (CriticalOn >= 12)
	{
		CriticalDamage = (rand() % 16)*_Attack.Lucky;
	}
	else
	{
		CriticalDamage = 0;
	}
	AttackPhysicalDamage = rand() % 50 + ((rand() % 11)*_Attack.Strength) + _Attack.WeaponDamage;
	AttackDamage = AttackPhysicalDamage + CriticalDamage;
	MissOrNotMissAttack = rand() % 10 + 1 * rand() % _Attack.Agility + 1;
	MissOrNotMissDefeat = rand() % 10 + 1 * rand() % _Defeat.Agility + 1;
	if (MissOrNotMissAttack > MissOrNotMissDefeat)
	{
		_Defeat.healthPoint -= AttackDamage;
		cout << _Attack.username << " ����� " << _Defeat.username << " " << AttackPhysicalDamage << " �����." << endl;
		if (CriticalDamage > 0)
		{
			cout << "����������� ����! �������� " << CriticalDamage << " ������������ �����" << endl << "����� ����: " << AttackDamage << endl;
		}

	}
	else
	{
		cout << _Defeat.username << " ��������� �� ����� " << _Attack.username << endl;
	}
}
void HardAttack(Player& _Attack, Player& _Defeat)
{
	int AttackDamage = 0, MissOrNotMissAttack = 0, MissOrNotMissDefeat = 0, CriticalDamage = 0, CriticalOn = 0, AttackPhysicalDamage = 0;
	srand(time(0));
	CriticalOn = rand() % 16;
	if (CriticalOn >= 12)
	{
		CriticalDamage = (rand() % 16)*_Attack.Lucky;
	}
	else
	{
		CriticalDamage = 0;
	}
	AttackPhysicalDamage = rand() % 100 + 25 + ((rand() % 11)*_Attack.Strength) + _Attack.WeaponDamage;
	AttackDamage = AttackPhysicalDamage + CriticalDamage;
	MissOrNotMissAttack = rand() % 5 + 1 * rand() % _Attack.Agility + 1;
	MissOrNotMissDefeat = rand() % 10 + 1 * rand() % _Defeat.Agility + 1;
	if (MissOrNotMissAttack > MissOrNotMissDefeat)
	{
		_Defeat.healthPoint -= AttackDamage;
		cout << _Attack.username << " ����� " << _Defeat.username << " " << AttackPhysicalDamage << " �����." << endl;
		if (CriticalDamage > 0)
		{
			cout << "����������� ����! �������� " << CriticalDamage << " ������������ �����" << endl << "����� ����: " << AttackDamage << endl;
		}

	}
	else
	{
		cout << _Defeat.username << " ��������� �� ����� " << _Attack.username << endl;
	}
}
void FastAttack(Player& _Attack, Player& _Defeat)
{
	int AttackDamage = 0, MissOrNotMissAttack = 0, MissOrNotMissDefeat = 0, CriticalDamage = 0, CriticalOn = 0, AttackPhysicalDamage = 0;
	srand(time(0));
	CriticalOn = rand() % 16;
	if (CriticalOn >= 12)
	{
		CriticalDamage = (rand() % 16)*_Attack.Lucky;
	}
	else
	{
		CriticalDamage = 0;
	}
	AttackPhysicalDamage = rand() % 25 + ((rand() % 11)*_Attack.Strength) + _Attack.WeaponDamage;
	AttackDamage = AttackPhysicalDamage + CriticalDamage;
	MissOrNotMissAttack = rand() % 15 + 1 * rand() % _Attack.Agility + 1;
	MissOrNotMissDefeat = rand() % 10 + 1 * rand() % _Defeat.Agility + 1;
	if (MissOrNotMissAttack > MissOrNotMissDefeat)
	{
		_Defeat.healthPoint -= AttackDamage;
		cout << _Attack.username << " ����� " << _Defeat.username << " " << AttackPhysicalDamage << " �����." << endl;
		if (CriticalDamage > 0)
		{
			cout << "����������� ����! �������� " << CriticalDamage << " ������������ �����" << endl << "����� ����: " << AttackDamage << endl;
		}

	}
	else
	{
		cout << _Defeat.username << " ��������� �� ����� " << _Attack.username << endl;
	}
}
void Intro()
{
	cout << endl << endl << endl << endl << endl << endl << endl ;
	cout << "                               *******************************************************" << endl;
	cout << "                               * #   # #####  ####  #   # #####     ##### #  # #   # *" << endl;
	cout << "                               * ##  #   #   #    # #   #   #      #      # #   # #  *" << endl;
	cout << "                               * # # #   #   #  ### #####   #       ####  ##     #   *" << endl;
	cout << "                               * #  ##   #   #    # #   #   #           # # #    #   *" << endl;
	cout << "                               * #   # #####  ####  #   #   #      #####  #  #   #   *" << endl;
	cout << "                               *******************************************************" << endl << endl << endl;
	cout << "                                                              ����������� [symbian767]" << endl;
	cout << "                                                                 ��������� [Flertenis]" << endl;
	cout << "                                                           ������ [Ghost_Alen aka gha]" << endl;
	cout << "                                                                            [��������]" << endl;
	Sleep(10000);  
}

void Gameover(char A[])
{
	PlaySound(MAKEINTRESOURCE(IDR_WAVE3), NULL, SND_RESOURCE | SND_ASYNC | SND_LOOP);
	system("cls");
	cout << "                      *******************************************************************" << endl;
	cout << "                      * #   #  ###  #   #     #   ####  #####   ####  #####   #   ####  *" << endl;
	cout << "                      *  # #  #   # #   #    # #  #   # #       #   # #      # #  #   # *" << endl;
	cout << "                      *   #   #   # #   #   #   # ####  #####   #   # ##### #   # #   # *" << endl;
	cout << "                      *   #   #   # #   #   ##### #  #  #       #   # #     ##### #   # *" << endl;
	cout << "                      *   #    ###   ###    #   # #   # #####   ####  ##### #   # ####  *" << endl;
	cout << "                      *******************************************************************" << endl << endl << endl;
	Message(A);
	cout << "���� ��������!" << endl;
	system("pause");

}

void TheEndLogo(char _A[])
{
	system("cls");
	PlaySound(MAKEINTRESOURCE(IDR_WAVE4), NULL, SND_RESOURCE | SND_ASYNC | SND_LOOP);
	cout << endl << endl << endl << endl << endl << endl << endl;
	cout << "                                *****************************************" << endl;
	cout << "                                * ##### #   # #####   ##### #   # ####  *" << endl;
	cout << "                                *   #   #   # #       #     ##  # #   # *" << endl;
	cout << "                                *   #   ##### #####   ##### # # # #   # *" << endl;
	cout << "                                *   #   #   # #       #     #  ## #   # *" << endl;
	cout << "                                *   #   #   # #####   ##### #   # ####  *" << endl;
	cout << "                                *****************************************" << endl << endl << endl;

	Message(_A);
}